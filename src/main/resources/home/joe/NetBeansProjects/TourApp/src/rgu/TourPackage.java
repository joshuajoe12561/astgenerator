public abstract class TourPackage implements Serializable {

    String description;
    String location;
    double tourCost;

    public TourPackage(String description, String location, double tourCost) {
        this.description = description;
        this.location = location;
        this.tourCost = tourCost;
    }

    public String getDescription() {
        return description;
    }

    public String getLocation() {
        return location;
    }

    public double getTourCost() {
        return tourCost;
    }

    void updateTourCost(double tourCost){
        if (tourCost > this.tourCost){
            this.tourCost = tourCost;
        }
    }

    @Override
    public String toString() {
        return "TourPackage{" + "description=" + description + ", location=" + location + ", tourCost=" + tourCost + '}';
    }

    abstract double getcost();
}