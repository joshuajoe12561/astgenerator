import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.RuleContext;
import org.antlr.v4.runtime.tree.ParseTree;
import org.repodriller.plugin.antlr.java8.Java8Lexer;
import org.repodriller.plugin.antlr.java8.Java8Parser;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;

public class ASTGenerator {
    private static String className;
    private static String classType;
    private static String superClass;
    public static String readFile() throws IOException {
        File file = new File("/home/joe/IdeaProjects/extractorapp/src/main/resources/home/joe/NetBeansProjects/TourApp/src/rgu/TourPackage.java");
        byte[] encoded = Files.readAllBytes(file.toPath());
        return new String(encoded, Charset.forName("UTF-8"));
    }

    public static void main(String args[]) throws IOException {

        String inputString = readFile();
        ANTLRInputStream input = new ANTLRInputStream(inputString);
        Java8Lexer lexer = new Java8Lexer(input);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        Java8Parser parser = new Java8Parser(tokens);
        ParserRuleContext ctx = parser.classDeclaration();
        printAST(ctx, false, 0);
        String type = superClass.substring(0, 6);
        if(type.equals("abstra")){
            System.out.println("class "+className + " abstract "+"class "+classType);
        }else if(type.equals("extend")){
            System.out.println("class "+className + " extends "+"class "+classType);
        }else if(type.equals("interf")){
            System.out.println("class "+className + " -- implements--> "+"interface "+superClass);
        }

    }

    private static void printAST(RuleContext ctx, boolean verbose, int indentation) {
        boolean toBeIgnored = !verbose && ctx.getChildCount() == 1 && ctx.getChild(0) instanceof ParserRuleContext;

        if (!toBeIgnored) {
            String ruleName = Java8Parser.ruleNames[ctx.getRuleIndex()];
            for (int i = 0; i < indentation; i++) {
                //System.out.print("  ");
            }
            if(ruleName.equals("simpleTypeName")){
                className = ctx.getText();
            }
            if(ruleName.equals("superclass")){
                superClass = ctx.getText();
            }
            if(ruleName.equals("classType")){
                classType = ctx.getText();
            }
            // System.out.println(ruleName + " -> " + ctx.getText());
        }

        for (int i = 0; i < ctx.getChildCount(); i++) {
            ParseTree element = ctx.getChild(i);
            if (element instanceof RuleContext) {
                printAST((RuleContext) element, verbose, indentation + (toBeIgnored ? 0 : 1));
            }
        }
        //System.out.println(className + " "+superClass);
    }
}
