
public class SightSeeingTour extends TourPackage {

    double ticketCost;

    public SightSeeingTour(double ticketCost, String description, String location, double tourCost) {
        super(description, location, tourCost);
        this.ticketCost = ticketCost;
    }

    public double getTicketCost() {
        return ticketCost;
    }

    public void updateTicketCost(double ticketCost) {

        if (ticketCost > this.ticketCost){
            this.ticketCost= ticketCost;
        }
    }

    @Override
    double getcost() {
        return ticketCost + tourCost;
    }

    @Override
    public String toString() {
        return "SightSeeingTour " + " ticketCost " + ticketCost + '}' + super.toString();
    }


}